package com.top.guod;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * project -
 *
 * @author guod
 * @version 3.0
 * @date 日期:2018/2/6 时间:16:17
 * @JDK JDK1.8
 * @Description 功能模块：
 */
@SpringBootApplication
@EnableEurekaServer
public class EurekaApplication {
    public static void main(String[] args) {
        SpringApplication.run(EurekaApplication.class, args);
    }
}
